import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { routing, appRoutingProviders } from './app.routing';

import { LoginModule } from './login/login.module';
import { CoreModule } from './core/core.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        routing,
        CoreModule,
        LoginModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        appRoutingProviders
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}