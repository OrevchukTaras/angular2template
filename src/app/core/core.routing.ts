import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";
import { CoreComponent } from "./core.component";

import { UserProfileComponent } from './user-profile/user-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { userProfileRoutes } from './user-profile/user-profile.routing';

export const coreRoutes: Routes = [
    {
        path: '',
        component: CoreComponent,
        children: [
            ...userProfileRoutes,
            { path: '', component: DashboardComponent }
        ]
    }
];

export const appRoutingProviders: any[] = [];
export const coreRouting: ModuleWithProviders = RouterModule.forChild(coreRoutes);
