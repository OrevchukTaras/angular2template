import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserProfileModule } from './user-profile/user-profile.module';

import { CoreComponent } from './core.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from './../shared/shared.module';


@NgModule({
    imports: [
        RouterModule,
        UserProfileModule,
        SharedModule
    ],
    exports: [
    ],
    declarations: [
        CoreComponent,
        DashboardComponent,
    ],
    providers: [
    ],
})
export class CoreModule { }