import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from './../../shared/shared.module';

import { UserProfileComponent } from './user-profile.component';
import { UserProfileSettingsComponent } from './user-profile-settings/user-profile-settings.component';
import { UserProfilePageComponent } from './user-profile-page/user-profile-page.component';

@NgModule({
    imports: [
        RouterModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        UserProfileComponent,
        UserProfileSettingsComponent,
        UserProfilePageComponent
    ],
    providers: [
    ],
})
export class UserProfileModule { }