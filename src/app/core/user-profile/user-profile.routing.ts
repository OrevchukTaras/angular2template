import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

import { UserProfileComponent } from "./user-profile.component";
import { UserProfileSettingsComponent } from './user-profile-settings/user-profile-settings.component';
import { UserProfilePageComponent } from './user-profile-page/user-profile-page.component';

export const userProfileRoutes: Routes = [
    {
        path: 'user-profile',
        component: UserProfileComponent,
        children: [
            { path: 'settings', component: UserProfileSettingsComponent },
            { path: '', component: UserProfilePageComponent }
        ]
    }
];

export const appRoutingProviders: any[] = [];
export const userProfileRouting: ModuleWithProviders = RouterModule.forChild(userProfileRoutes);
