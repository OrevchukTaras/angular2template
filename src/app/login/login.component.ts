import { Component } from "@angular/core";
import { LoginService } from "./login.service";
import { LoginModel } from "./login.model";
import { LoginDataModel } from "./loginData.model";

@Component({
    selector: 'login',
    templateUrl: './app/login/login.html',
    providers: [LoginService, LoginModel]
})

export class LoginComponent {
    loginModel: LoginModel;
    loginData: LoginDataModel;

    constructor(private _loginService: LoginService) {
        this.loginModel = new LoginModel();
        this.loginData = new LoginDataModel();
    };

    private errorLogin(err: any) {
        console.log(err);
    };
    private successLogin(res: any) {
        this.loginData = res;
    };

    login() {
        this._loginService.login(this.loginModel)
            .subscribe(
            data => this.successLogin(data),
            this.errorLogin);
    };
}