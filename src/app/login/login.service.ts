import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { LoginModel } from "./login.model";
import { LoginDataModel } from "./loginData.model";

import config = require('../../app/config');
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {
    constructor(private http: Http) {
    }

    login(loginModel: LoginModel) {
        var credentials = "grant_type=password&username=" + loginModel.username + "&password=" + loginModel.password + "&userType=Staff";

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(config.apiUrl + 'token', credentials, {
            headers: headers
        })
            .map(response => this.extractData(response))
            .catch(error => this.handleError(error));
    }

    private extractData(response: Response) {
        return response.json();
    }
    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        return Observable.throw(errMsg);
    }
}