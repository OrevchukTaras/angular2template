export class LoginDataModel {
    username: string;
    token_type: string;
    permissions: string;
    id: string;
    expiresIn: number;
    access_token: string;
}