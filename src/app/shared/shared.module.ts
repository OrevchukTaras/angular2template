import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
@NgModule({
    imports: [CommonModule],
    declarations: [FooterComponent, HeaderComponent],
    exports: [
        HeaderComponent,
        FooterComponent,
        CommonModule,
        FormsModule]
})
export class SharedModule { }
