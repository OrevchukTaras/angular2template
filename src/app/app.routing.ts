import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

import { coreRoutes } from './core/core.routing';
import { loginRoutes } from './login/login.routing';

const appRoutes: Routes = [
    ...coreRoutes,
    ...loginRoutes];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
