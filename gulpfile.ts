"use strict";

const gulp = require("gulp");
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("tsconfig.json");
const tslint = require('gulp-tslint');
const jade = require('gulp-jade');
const less = require('gulp-less');

/**
 * Compile jade to html.
 */
gulp.task('jade', function () {
    return gulp.src('src/**/*.jade')
        .pipe(jade())
        .pipe(gulp.dest("build"));
});

/**
 * Compile less files.
 */
gulp.task('less', function () {
    return gulp.src('src/less/main.less')
        .pipe(less())
        .pipe(gulp.dest('build/css/'));
});

/**
 * Remove build directory.
 */
gulp.task('clean', (cb) => {
    return del(["build"], cb);
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', () => {
    return gulp.src("src/**/*.ts")
        .pipe(tslint({
            formatter: 'prose'
        }))
        .pipe(tslint.report());
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", ["tslint"], () => {
    let tsResult = gulp.src("src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));
    return tsResult.js
        .pipe(sourcemaps.write(".", { sourceRoot: '/src' }))
        .pipe(gulp.dest("build"));
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", () => {
    return gulp.src([
        "src/**/*",
        "!**/*.ts",
        "!**/*.less",
        "!src/less{,/**}",
        "!**/*.jade"
    ])
        .pipe(gulp.dest("build"));
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", () => {
    return gulp.src([
        'core-js/client/shim.min.js',
        'systemjs/dist/system-polyfills.js',
        'systemjs/dist/system.src.js',
        'reflect-metadata/Reflect.js',
        'rxjs/**/*.js',
        'zone.js/dist/**',
        '@angular/**/bundles/**'
    ], { cwd: "node_modules/**" }) /* Glob required here. */
        .pipe(gulp.dest("build/lib"));
});

/**
 * Watch for changes in files.
 */
gulp.task('watch', function () {
    gulp.watch(["src/**/*.ts"], ['compile']);

    gulp.watch(["src/**/*.jade"], ['jade']);

    gulp.watch(["src/**/*.less"], ['less']);

    // gulp.watch(["src/**/*.html", "src/**/*.css"], ['resources']);
});

/**
 * Build the project.
 */
gulp.task("build", ['compile', 'resources', 'libs', 'jade', 'less'], () => {
    console.log("Building the project ...");
});